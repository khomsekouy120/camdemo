
class TransactionData {
  String title;
  String description;
  String amount;

  TransactionData({
    required this.title,
    required this.description,
    required this.amount,
  });
}

List<TransactionData> transactions = [
  TransactionData(
      title: "Lorem Ipsum Company",
      description: "Reveived payment",
      amount: "2,030.80"),
  TransactionData(
      title: "Auctor Elit Ltd.",
      description: "Transfer money",
      amount: "450.00"),
  TransactionData(
      title: "Lectus Sit Amet est",
      description: "Gas & electricity payment",
      amount: "1,500.00"),
  TransactionData(
      title: "Congue Quisque",
      description: "withdraw money",
      amount: "1,500.00")
];
