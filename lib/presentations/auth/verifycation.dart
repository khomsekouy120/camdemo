import 'package:camdemo/core/constant.dart';
import 'package:camdemo/presentations/components/app_text_field.dart';
import 'package:camdemo/presentations/route/route_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class VerificationCode extends StatefulWidget {
  const VerificationCode({Key? key}) : super(key: key);

  @override
  State<VerificationCode> createState() => _VerificationCodeState();
}

class _VerificationCodeState extends State<VerificationCode> {
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 44,
        elevation: 0.0,
        backgroundColor: const Color.fromARGB(255, 27, 49, 67),
        title: Row(
          children: const [
            Text('Resset Password'),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              child: Column( 
            children: [  
                    Container(
                // padding: const EdgeInsets.all(10),
                child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 5),
                  child: const Image(
                    image: AssetImage(
                      'images/verifycode.png',
                    ),
                    width: 300,
                    height: 250,
                    fit: BoxFit.fill,
                  ),
                ),
                 Container(
                  padding: const EdgeInsets.all(15),
                  child: const Text('Verify Code', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
                ),

                Container(
                  padding: const EdgeInsets.all(15),
                  child: const Text('Enter the Code sent to +885 969 432 711'),
                ),
              Container(  
                padding: const EdgeInsets.all(35),
                  child:  Row(  
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [  
                      SizedBox( 
                        height: 68, 
                        width:64,
                        child: TextField(  
                          style:Theme.of(context).textTheme.headline6,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          inputFormatters: [  
                            LengthLimitingTextInputFormatter(1),
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      ),
                       SizedBox( 
                        height: 68, 
                        width:64,
                        child: TextField(  
                          style:Theme.of(context).textTheme.headline6,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          inputFormatters: [  
                            LengthLimitingTextInputFormatter(1),
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      ),
                       SizedBox( 
                        height: 68, 
                        width:64,
                        child: TextField(  
                          style:Theme.of(context).textTheme.headline6,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          inputFormatters: [  
                            LengthLimitingTextInputFormatter(1),
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      ),
                       SizedBox( 
                        height: 68, 
                        width:64,
                        child: TextField(  
                          style:Theme.of(context).textTheme.headline6,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          inputFormatters: [  
                            LengthLimitingTextInputFormatter(1),
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      )
                    ],
                  ),
              ), 

            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.fromLTRB(33, 0, 0, 0),
              child: Row(  
                children:  [  
                 const Text('Dont you recive the OTP?'), 
                  TextButton(
                    child: const Text('Resend Code'),
                    onPressed:(){ 
                    }
                    )
                ],
              ),

            ),
                Container(
                  padding: const EdgeInsets.all(0),
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all(
                            const EdgeInsets.fromLTRB(155, 20, 155, 20)),
                        backgroundColor:
                            MaterialStateProperty.all(kContentColorLightTheme),
                      ),
                      child: const Text(
                        'Verify',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                      onPressed: () {
                        // if (formKey.currentState!.validate()) {
                          Navigator.pushNamed(context, RouteName.login);
                        // }
                      },
                    ),
                  ],
                )),
              ],
            ))
            ],       
              )),
    
          ],
        ),
      ),
    );
  }
}
