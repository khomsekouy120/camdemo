// import 'package:flutter/cupertino.dart';
// import 'dart:js';

import 'package:camdemo/core/constant.dart';
import 'package:camdemo/presentations/auth/verifycation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'verifycation.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 44,
        elevation: 0.0,
        backgroundColor: const Color.fromARGB(255, 27, 49, 67),
        title: Row(
          children: const [
            Text('Resset Password'),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
         
          children: [
            Container(
                // padding: const EdgeInsets.all(10),
                child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 5),
                  child: const Image(
                    image: AssetImage(
                      'images/ressetpassword.png',
                    ),
                    width: 300,
                    height: 250,
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.all(30),
                  child: const Text(
                    'Enter the phone number associated with your account.',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(right: 280),
                  child: const Text('Phone Number'),
                ),

                Form(
                    key: formKey,
                  child: Container(
                    padding: const EdgeInsets.all(25),
                    child: TextFormField(
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter Your Phone Number',
                        prefixIcon: Padding(
                          padding: EdgeInsets.only(
                              top: 0), // add padding to adjust icon
                          child: Icon(Icons.mobile_screen_share_rounded),
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Enter Your Phone Number';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                    ),
                  ),
                ),
                
                Container(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all(
                            const EdgeInsets.fromLTRB(30, 20, 25, 20)),
                        backgroundColor:
                            MaterialStateProperty.all(kContentColorLightTheme),
                      ),
                      child: const Text(
                        'Send',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const VerificationCode()));
                        }
                      },
                    ),
                  ],
                )),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
