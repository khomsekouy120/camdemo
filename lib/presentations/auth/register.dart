

import 'package:camdemo/core/constant.dart';
import 'package:camdemo/presentations/route/route_name.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../core/constant.dart';


class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final formKey = GlobalKey<FormState>();

  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            Container(
              width: double.infinity,
              height: 201,
              color: kContentColorLightTheme,
              padding: const EdgeInsets.all(50),
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.house,
                      color: kContentColorDarkTheme,
                      size: 80,
                    ),
                    Icon(
                      Icons.link,
                      color: kContentColorDarkTheme,
                      size: 80,
                    ),
                    Icon(
                      Icons.mobile_friendly,
                      color: kContentColorDarkTheme,
                      size: 80,
                    ),
                  ],
                ),
                const Text(
                  'Connect to your bank account',
                  style: TextStyle(color: kContentColorDarkTheme),
                )
              ]),
            ),
            Form(
              
                key: formKey,
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(15),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: "Your Names",
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty || value == 6) {
                            return 'input your usernames';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(15),
                        child: TextFormField(
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Enter your bank number"),
                          validator: (value) {
                            if (value == null || value.isEmpty || value == 6) {
                              return 'input your bank number';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        )),
                    Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(15),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: "enter a valid email",
                        ),
                        validator: (val) =>
                            val!.isEmpty || !val.contains("@gmail.com")
                                ? "Email"
                                : null,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(15),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: "Password",
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty || value == 6) {
                            return 'input right password';
                          }
                          return null;
                        },
                        obscureText: true,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(15),
                      child: const Text(
                        'User 6 characters with a mix of letters, numbers & symbols.',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                    Container(         
                       alignment: Alignment.center,
                      padding: const EdgeInsets.all(15),
                      child: Row(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Transform.scale(
                              scale: 1.5,
                              child: Checkbox(

                                checkColor: const Color.fromARGB(255, 27, 23, 23),
                                fillColor: MaterialStateProperty.all(Colors.blue),
                                value: isChecked,
                                onChanged: (bool? value) {
                                  setState(() {
                                    isChecked = value!;
                                  });
                                },
                              ),
                            ),
                              const Text(
                              'By signing up, ',
                              style: TextStyle(fontSize: 17.0),
                            ),
                          ]),
                    ),
                    Container(
                        height: 60,
                        // padding: const EdgeInsets.all(15),
                        child: Row(
                   
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              style: ButtonStyle(
                                padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(20, 20, 25,20)),
                                backgroundColor: MaterialStateProperty.all(
                                    kContentColorLightTheme),
                              ),
                              child: const Text('SignUp', style: TextStyle(color: Colors.white,  fontSize: 20,),),
                              onPressed: () {
                                if (formKey.currentState!.validate()) {
                                  //  ScaffoldMessenger.of(context).showSnackBar(
                                  //     const SnackBar(content: Text('Processing Data')),
                                  //   );
                                }
                              },
                            ),
                            const Text('or'),
                            ElevatedButton(
                              style: ButtonStyle(
                                padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(20, 20, 25,20)),

                                backgroundColor: MaterialStateProperty.all(
                                    kContainerBackgroundColor),
                              ),
                              child: const Text(
                                'CANCEL',
                                style: TextStyle(color: Colors.black,  fontSize: 20,),
                              ),
                              onPressed: () {
                                //  if (formKey.currentState!.validate()) {

                                // }
                              },
                            ),
                          ],
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('Aleadly signed up?'),
                        TextButton(
                          child: const Text(
                            'Login',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.blue,
                            ),
                          ),
                          onPressed: () {
                             Navigator.pushNamed(context, RouteName.login);
                          },
                          
                        ),
                       
                      
                    ],
                  )
                ],
              ))
        ],
      ),
    );
  }
}
