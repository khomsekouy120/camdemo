import 'package:flutter/material.dart';
import 'package:camdemo/core/constant.dart';

import '../route/route_name.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            //Head bar
            Container(
              width: double.infinity,
              color: kContentColorLightTheme,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  SizedBox(
                    height: 80,
                  ),
                  Text(
                    'WELCOME!',
                    style: TextStyle(fontSize: 40, color: Colors.white),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Icon(
                    Icons.home,
                    size: 150,
                    color: kPrimaryColorLight,
                  ),
                  SizedBox(
                    height: 40,
                  ),
                ],
              ),
            ),
            //Field input
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 40),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    //Email field input
                    Center(
                      child: TextFormField(
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Username or Email'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please input this field!";
                          } else if (!RegExp(
                                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                              .hasMatch(value)) {
                            return "These value are not the correct format email!";
                          }
                          return null;
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    //Password field input
                    Center(
                      child: TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(), hintText: 'Password'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please input this field!";
                          } else if (value.toString().length < 6) {
                            return "Password must be more than 5 charactor!";
                          }
                          return null;
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    //Login Button
                    Center(
                      child: ElevatedButton(
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.fromLTRB(40, 5, 40, 5)),
                            backgroundColor: MaterialStateProperty.all(
                                kContentColorLightTheme)),
                        onPressed: () {
                          if (_formKey.currentState!.validate()){
                            Navigator.pushNamed(context, RouteName.payment);
                          }
                        },
                        child: const Text(
                          'LOG IN',
                          style: TextStyle(color: kPrimaryColorLight),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    // Forgot Password Textbutton
                    Center(
                      child: TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, RouteName.resetpassword);
                        },
                        child: const Text(
                          'Forgot Password?',
                          style: TextStyle(
                              fontSize: kRadiusThirdContainer,
                              color: kPrimaryColor,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                    ),
                    // Register New Account TextButton
                    Align(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            'New to Bank Apps? ',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          TextButton(
                              onPressed: () {
                                Navigator.pushNamed(context, RouteName.register);
                              },
                              child: const Text(
                                'Sign Up',
                                style: TextStyle(
                                    fontSize: 16,
                                    color: kPrimaryColor,
                                    decoration: TextDecoration.underline),
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
