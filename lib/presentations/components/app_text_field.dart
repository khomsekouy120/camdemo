import 'package:flutter/material.dart';

import '../../core/constant.dart';

class AppTextField extends StatefulWidget {
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final String hintText;
  final TextInputType? inputType;
  final bool isPassword;
  final String errorText;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final int? maxLines;
  final double borderRadius;
  final bool rectangle;
  final double height;
  final double verticalPadding;
  final double horizotalPadding;
  final Function(String)? onChange;

  const AppTextField({
    Key? key,
    this.controller,
    this.focusNode,
    this.hintText = '',
    this.isPassword = false,
    this.inputType,
    this.errorText = '',
    this.prefixIcon,
    this.suffixIcon,
    this.onChange,
    this.maxLines = 1,
    this.borderRadius = kRadiusControl,
    this.rectangle = false,
    this.height = 40,
    this.verticalPadding = kDefaultPadding / 3,
    this.horizotalPadding = kDefaultPadding,
  }) : super(key: key);

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  String _errorMessage = '';
  double _height = 0;
  Color _borderColor = Colors.black;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
       SizedBox(
            height: widget.height,
            child: TextFormField(
              focusNode: widget.focusNode,
              controller: widget.controller,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  setState(() {
                    _height = 25;
                    _errorMessage = widget.errorText;

                    _borderColor = kErrorColor;
                  });
                } else {
                  setState(() {
                    _height = 0;
                    _errorMessage = '';
                    _borderColor = Colors.black;
                  });
                }
                return null;
              },
              onFieldSubmitted: (value) {
                if (value.isEmpty) {
                  setState(() {
                    _height = 25;
                    _errorMessage = widget.errorText;
                    _borderColor = kErrorColor;
                  });
                } else {
                  setState(() {
                    _height = 0;
                    _errorMessage = '';
                    _borderColor = Colors.black;
                  });
                }
              },
              obscureText: widget.isPassword,
              keyboardType: widget.inputType ?? TextInputType.text,
              maxLines: widget.maxLines,
              onChanged: widget.onChange,
              decoration: InputDecoration(
                  // isDense: true,
                  icon: widget.prefixIcon,
                  border: InputBorder.none,
                  hintText: widget.hintText,
                  suffixIcon: widget.suffixIcon),
            ),
        ),
        AnimatedContainer(
          margin: const EdgeInsets.only(
              left: kDefaultPadding, top: kDefaultPadding / 2),
          height: _height,
          duration: const Duration(milliseconds: 250),
          child: Text(
            _errorMessage,
            style: Theme.of(context)
                .textTheme
                .bodyText2!
                .copyWith(color: kErrorColor),
          ),
        ),
      ],
    );
  }
}
