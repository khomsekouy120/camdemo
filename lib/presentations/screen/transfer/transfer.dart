import 'package:camdemo/presentations/route/route_name.dart';
import 'package:flutter/material.dart';
import 'package:camdemo/core/constant.dart';
import 'package:flutter/services.dart';

import '../../../core/constant.dart';

class Transfer extends StatefulWidget {
  const Transfer({Key? key}) : super(key: key);

  @override
  State<Transfer> createState() => _TransferState();
}

class _TransferState extends State<Transfer> {

  final _formKey = GlobalKey<FormState>();

  String dropdownvalue = 'Dollar';

  // List of items in dropdown menu
  var items = [
    'Dollar',
    'Real',
    'Bart',
    'Pond',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: kContentColorLightTheme,
        leading: SizedBox(
          child: Row(
            children: [
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.menu, size: 30,),
                ),
              ),
              const Spacer(),
              Expanded(
                child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.keyboard_arrow_left, size: 30,)),
              ),
            ],
          ),
        ),
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.settings)),
        ],
        title: const Text(
          'TRANSACTION',
          style: TextStyle(letterSpacing: 2, color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(30),
        child: Column(children: [
          //Card view
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.money_rounded,
                  color: kContentColorLightTheme,
                  size: 150,
                ),
                Column(
                  children: const [
                    Icon(
                      Icons.arrow_forward_rounded,
                      color: kContentColorLightTheme,
                      size: 60,
                    ),
                    Icon(
                      Icons.arrow_back_rounded,
                      color: kContentColorLightTheme,
                      size: 60,
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            child: Form(
              key: _formKey,
              child: Column(children: [
                //From back account
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 20, bottom: 10),
                        alignment: Alignment.topLeft,
                        child: const Text(
                          'From Bank Account',
                          style: TextStyle(
                              color: kContentColorLightTheme,
                              fontSize: 16,
                              letterSpacing: 1),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(3),
                        ),
                        child: Text('00 123 555', style: TextStyle(color: Colors.black87, fontSize: 16),),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 20, bottom: 10),
                        alignment: Alignment.topLeft,
                        child: const Text(
                          'To Bank Account',
                          style: TextStyle(
                              color: kContentColorLightTheme,
                              fontSize: 16,
                              letterSpacing: 1),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Accont number'),
                        keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                        ],
                        validator: (value) {
                          if(value==null || value.isEmpty){
                            return 'Please input this field!';
                          }else if(value.toString().length == 8){
                            return null;
                          }
                          return "Pleas check account number!";
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 20, bottom: 10),
                        alignment: Alignment.topLeft,
                        child: const Text(
                          'Amount',
                          style: TextStyle(
                              color: kContentColorLightTheme,
                              fontSize: 16,
                              letterSpacing: 1),
                        ),
                      ),
                      Row(
                        children: [
                          // Expanded(
                          Container(
                            width: 90,
                            height: 58,
                            margin: EdgeInsets.only(right: 10),
                            padding: const EdgeInsets.symmetric(horizontal: 5, ),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(3),
                            ),
                            child: DropdownButton(
                              underline: const SizedBox(),

                              // Initial Value
                              value: dropdownvalue,

                              // Down Arrow Icon
                              icon: const Icon(Icons.keyboard_arrow_down),

                              // Array list of items
                              items: items.map((String items) {
                                return DropdownMenuItem(
                                  value: items,
                                  child: Text(items),
                                );
                              }).toList(),
                              // After selecting the desired option,it will
                              // change button value to selected value
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownvalue = newValue!;
                                });
                              },
                            ),
                          ),
                          // ),
                          Expanded(
                            child: TextFormField(
                              textAlign: TextAlign.end,
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: '0.00'),
                              validator: (value) {
                                if(value==null||value.isEmpty){
                                  return "Please input this field";
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.digitsOnly
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 20, bottom: 10),
                        alignment: Alignment.topLeft,
                        child: const Text(
                          'Messages',
                          style: TextStyle(
                              color: kContentColorLightTheme,
                              fontSize: 16,
                              letterSpacing: 1),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          if(_formKey.currentState!.validate()){

                          }
                        },
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.fromLTRB(40, 5, 40, 5)),
                            backgroundColor: MaterialStateProperty.all(
                                kContentColorLightTheme)),
                        child: const Text(
                          'SEND',
                          style:
                              TextStyle(color: Colors.white, letterSpacing: 1),
                        )),
                    const SizedBox(
                      width: 30,
                    ),
                   
                    OutlinedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, RouteName.exchange);
                        },
                        style: OutlinedButton.styleFrom(
                            padding: const EdgeInsets.fromLTRB(30, 5, 30, 5),
                            side: const BorderSide(
                                width: 1.0, color: kContentColorLightTheme)),
                        child: const Text(
                          'EXCHANGE',
                          style: TextStyle(
                              color: kContentColorLightTheme, letterSpacing: 1),
                        ))
                  ],
                )
              ]),
            ),
          )
        ]),
      ),
    );
  }
}