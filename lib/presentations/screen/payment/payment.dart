import 'package:camdemo/presentations/route/route_name.dart';
import 'package:flutter/material.dart';
import 'package:camdemo/core/constant.dart';

class Payment extends StatefulWidget {
  const Payment({Key? key}) : super(key: key);

  @override
  State<Payment> createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: kContentColorLightTheme,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.menu),
        ),
        actions: [
          IconButton(onPressed: (){Navigator.pushNamed(context, RouteName.transaction);}, icon: const Icon(Icons.qr_code_scanner_outlined)),
          IconButton(onPressed: () {}, icon: const Icon(Icons.settings)),
        ],
        title: const Text(
          'PAYMENT',
          style: TextStyle(letterSpacing: 2, color: Colors.white),
        ),
      ),
      body: Center(
        child: Column(
          children: [

            //Header
            Container(
              color: kContentColorLightTheme,
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
              child: Row(children: [
                Container(
                  height: 90,
                  width: 90,
                  // padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50)),
                  child: IconButton(
                    onPressed: () {
                      Navigator.pushNamed(context, RouteName.account);
                    }, 
                    icon: const Icon(Icons.person, size: 70, color: kContentColorLightTheme,),
                    
                    // child: Icons.person,
                    // size: 80,
                    // color: kContentColorLightTheme,
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      'BALANCE',
                      style: TextStyle(
                          color: kPrimaryColor, fontSize: 16, letterSpacing: 1),
                    ),
                    Text(
                      '\$4,180.20',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                )
              ]),
            ),
          
            //Options user choose
            Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(30, 30, 30, 10),
              child: 
                    Wrap(
                      runSpacing: 20,
                      alignment: WrapAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {

                          },
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(50)),
                              child:const Icon(Icons.water_drop, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: Text("Watter", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 244, 211, 49),
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(Icons.lightbulb_outline, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: Text("Electricity", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Colors.orange,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(Icons.gas_meter_outlined, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: Text("Gas", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Colors.pink[500],
                                  borderRadius: BorderRadius.circular(50)),
                              child: const Icon(Icons.shopping_bag, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: const Text("Shopping", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: kContentColorLightTheme,
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(Icons.phone_android, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: Text("Phone", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, RouteName.create_card);
                          },
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Colors.green[800],
                                  borderRadius: BorderRadius.circular(50)),
                              child: const Icon(Icons.credit_card, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: Text("Card", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Colors.teal[800],
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(Icons.shield_moon, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: const Text("Insurance", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Colors.purple[700],
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(Icons.home, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: Text("Mortgage", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                        GestureDetector(
                          child: Column(children: [
                            Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                  color: Colors.grey[800],
                                  borderRadius: BorderRadius.circular(50)),
                              child: Icon(Icons.list_alt, size: 50, color: Colors.white,),
                            ),
                            TextButton(onPressed: (){}, child: Text("Other", style:const TextStyle(color: Colors.black54, fontSize: 16),))
                          ],)
                        ),
                      ],
                    )
            ),

            //Text button on option more
            Container(
              padding: EdgeInsets.only(right: 30),
              alignment: Alignment.topRight,
              child: TextButton(
                onPressed: (){},
                child: Text('more >>', style: TextStyle(color: Colors.blue, fontSize: 16),),
              ),
            )
          ],
        ),
      ),
    );
  }
}
