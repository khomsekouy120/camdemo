import 'package:camdemo/presentations/route/route_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:camdemo/core/constant.dart';

class CreateCard extends StatefulWidget {
  const CreateCard({Key? key}) : super(key: key);

  @override
  State<CreateCard> createState() => _CreateCardState();
}

class _CreateCardState extends State<CreateCard> {
  final _formKey = GlobalKey<FormState>();
  final items = [
    '+880',
    '+883',
    '+884',
    '+885',
    '+888',
    '+123',
  ];
  String? value = '+885';

  DateTime date = DateTime(2022, 12, 24);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kContentColorLightTheme,
        leading: SizedBox(
          child: Row(
            children: [
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.menu),
                ),
              ),
              const SizedBox(
                width: 30,
              ),
              Expanded(
                child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.keyboard_arrow_left)),
              )
            ],
          ),
        ),
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.settings)),
        ],
        title: const Text(
          'ADD CARD',
          style: TextStyle(letterSpacing: 2, color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //Head bar
            Container(
                margin: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Container(
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [
                            0.3,
                            1
                          ],
                          colors: [
                            Color.fromARGB(255, 32, 154, 198),
                            Color.fromARGB(255, 127, 221, 255)
                          ]),
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  padding: const EdgeInsets.all(20),
                  width: 240,
                  height: 140,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const Text(
                        'BANK NAME',
                        style: TextStyle(
                          color: kContentColorLightTheme,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1,
                        ),
                        textAlign: TextAlign.start,
                      )
                    ],
                  ),
                )),
            //Field input
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 40),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    // Container(
                      // key: _formKey,
                      Column(
                        children: [
                          //Name field input
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'YOUR NAME',
                              ),
                              TextFormField(
                                decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: "NAME",
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 0, 10, 0)),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Please input this field";
                                  }
                                  return null;
                                },
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),

                          //Card Number field input
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text('CARD NUMBER'),
                              TextFormField(
                                decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: "INSERT YOUR CARD NUMBER",
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 10)),
                                keyboardType: TextInputType.number,
                                            inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                ],
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Please input this field";
                                  }
                                  return null;
                                },
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),

                          //Expired date
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text('EXPIRED DATE'),
                              Container(
                                width: double.infinity,
                                // height: 30,                                          padding: const EdgeInsets.only(left: 5, right: 5),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(3),
                                ),
                                child: Center(
                                  child: Row(
                                    children: [
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        '${date.day}/${date.month}/${date.year}',
                                        style: const TextStyle(fontSize: 18),
                                      ),
                                      const Spacer(),
                                      IconButton(
                                          onPressed: () async {
                                            DateTime? newDate =
                                                await showDatePicker(
                                                    context: context,
                                                    initialDate: date,
                                                    firstDate: DateTime(1900),
                                                    lastDate: DateTime(2100));
                                            if (newDate == null) return;

                                            setState(() {
                                              date = newDate;
                                            });
                                          },
                                          icon: const Icon(
                                            Icons.date_range,
                                            color: Colors.black54,
                                          ))
                                    ],
                                  ),
                                  // child: Text('hello', style: TextStyle(fontSize: 18),),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),

                          //Password
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text('PASSWORD'),
                              TextFormField(
                                obscureText: true,
                                decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: "*********",
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5)),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Please input this field";
                                  } else if (value.toString().length < 5) {
                                    return "Password most be more then 5 charactors";
                                  }
                                  return null;
                                },
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),

                          // Phone Number
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("PHONE NUMBER"),
                              Row(children: [
                                //Codebar phone number country
                                Container(
                                  padding:
                                      const EdgeInsets.only(left: 5, right: 5),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                  child: DropdownButton<String>(
                                    underline: const SizedBox(),
                                    menuMaxHeight: 150,
                                    value: value,
                                    items: items.map(buildMenuItem).toList(),
                                    onChanged: (value) => setState(
                                      () => this.value = value,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                //Phone number
                                Container(
                                  child: Expanded(
                                      child: TextFormField(
                                    // keyboardType: const TextInputType.numberWithOptions(decimal: true),
                                    // inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9.,]')),],
                                    decoration: const InputDecoration(
                                        border: OutlineInputBorder(),
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 5)),
                                    keyboardType: TextInputType.number,
                                                inputFormatters: <TextInputFormatter>[
                                        FilteringTextInputFormatter.digitsOnly
                                    ],

                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return "Please input this field";
                                      } else if (value.toString().length < 9 || value.toString().length > 9) {
                                        return "Please check your number";
                                      }
                                      return null;
                                    },
                                  )),
                                )
                              ])
                            ],
                          ),
                          const SizedBox(
                            height: 30,
                          ),

                          //Description
                          const Text(
                              "* Nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non."),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text(
                              "* Nunc faucibus a pellentesque sit amet porttitor eget dolor morbi non.")
                        ],
                      ),
                    // ),
                    const SizedBox(
                      height: 30,
                    ),

                    //Login Button
                    Center(
                      child: ElevatedButton(
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.fromLTRB(40, 5, 40, 5)),
                            backgroundColor: MaterialStateProperty.all(
                                kContentColorLightTheme)),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            // Navigator.pushNamed(context, RouteName.account);
                          }
                        },
                        child: const Text(
                          'LINK CARD',
                          style: TextStyle(
                              color: kPrimaryColorLight, letterSpacing: 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Container(
          child: Text(item),
        ),
      );
}
