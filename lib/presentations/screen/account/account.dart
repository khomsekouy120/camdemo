import 'package:camdemo/model/transaction.dart';
import 'package:camdemo/presentations/route/route_name.dart';
import 'package:flutter/material.dart';
import 'package:camdemo/core/constant.dart';

class Account extends StatefulWidget {
  const Account({Key? key}) : super(key: key);

  @override
  State<Account> createState() => _AccountState();
}

class _AccountState extends State<Account> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
              color: kContentColorLightTheme,
              padding: const EdgeInsets.fromLTRB(10, 50, 10, 10),
              child: Column(
                children: [
                  //Icons button menu
                  Row(children: [
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.menu,
                          size: 30,
                          color: Colors.white,
                        )),
                    IconButton(
                      onPressed: (){
                        Navigator.pop(context);
                      }, 
                      icon:const Icon(
                        Icons.keyboard_arrow_left,
                        size: 30,
                        color: Colors.white,
                      )
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: (){
                        Navigator.pushNamed(context, RouteName.edite_account);
                      }, 
                      icon: const Icon(
                        Icons.edit_calendar_sharp,
                        size: 30,
                        color: Colors.white,
                      )
                    ),
                    SizedBox(width: 10,),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.settings,
                          size: 30,
                          color: Colors.white,
                        )),
                  ]),
                  // User profile
                  Container(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Column(
                      children: [
                        const Icon(
                          Icons.people,
                          size: 100,
                          color: Colors.white,
                        ),
                        const Text(
                          'YOUR NAME',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 1),
                        ),
                        const Text(
                          'your-email@email.com',
                          style: TextStyle(color: Colors.white, fontSize: 16, letterSpacing: 1),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0)),
                            child: Container(
                              width: 270,
                              padding: const EdgeInsets.all(20),
                              // height: 200,
                              child: Column(
                                children: [
                                  const Text(
                                    'BALANCE',
                                    style: TextStyle(
                                        fontSize: 16, color: kPrimaryColor, letterSpacing: 1),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  const Text(
                                    "\$4,180.20",
                                    style: TextStyle(
                                        fontSize: 35,
                                        color: kContentColorLightTheme,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  ElevatedButton(
                                    onPressed: () {
                                      Navigator.pushNamed(context, RouteName.transfer);
                                    },
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                kContentColorLightTheme),
                                        padding: MaterialStateProperty.all(
                                            const EdgeInsets.fromLTRB(
                                                30, 5, 30, 5))),
                                    child: const Text('TRANSFER', style: TextStyle(letterSpacing: 2),),
                                  ),
                                ],
                              ),
                            ))
                      ],
                    ),
                  ),
                ],
              )
          ),
          //Description menu
          Container(
              padding: const EdgeInsets.all(20),
              child: const Center(
                child: Text(
                  'LATEST TRANSACTIONS',
                  style: TextStyle(
                      color: kPrimaryColor, fontSize: 16, letterSpacing: 1, fontWeight: FontWeight.bold),
                ),
              )
          ),
          //Transaction list view
          Column(
            children: transactions.map((transaction) {
              return Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Row(
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      margin:const EdgeInsets.fromLTRB(0, 10, 10, 10),
                      decoration: const BoxDecoration(
                          color: kContentColorLightTheme,
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                    ),
                    
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          transaction.title,
                          style:
                                const TextStyle(color: Colors.black54, fontSize: 16),
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          transaction.description,
                          style:
                              const TextStyle(color: Colors.black54, fontSize: 14),
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                    const Spacer(),
                    Text(
                      "\$${transaction.amount}",
                      style:
                          const TextStyle(color: Colors.black54, fontSize: 16),
                    )
                  ],
                ),
              );
            }).toList(),
          ),

          Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 20, 0),
            alignment: Alignment.centerRight,
            child: TextButton(
              onPressed: () {
                Navigator.pushNamed(context, RouteName.transaction_history);
              },
              child: const Text(
                'more >>',
                style: TextStyle(color: kPrimaryColor, fontSize: 16),
              ),
            ),
          )
        ],
      ),
    );
  }
}
