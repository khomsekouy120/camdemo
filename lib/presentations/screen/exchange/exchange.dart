import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Exchange extends StatefulWidget {
  const Exchange({Key? key}) : super(key: key);

  @override
  State<Exchange> createState() => _ExchangeState();
}

String dropdownvalue = 'R';

class _ExchangeState extends State<Exchange> {
  var items = [
    'R',
    '\$',
    '£',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 27, 49, 67),
        title: Row(
          children: [
            IconButton(
              icon: const Icon(Icons.keyboard_arrow_left),
              iconSize: 30,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            const Spacer(),
            const Text(
              'EXCHANGE',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const Spacer(),
            IconButton(
              icon: const Icon(Icons.settings),
              iconSize: 30,
              onPressed: () {},
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.fromLTRB(0, 50, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: const [
                      Icon(
                        Icons.monetization_on,
                        size: 60,
                        color: Color.fromARGB(255, 36, 73, 104),
                      ),
                    ],
                  ),
                  Column(
                    children: const [
                      Icon(
                        Icons.arrow_forward,
                        size: 30,
                        color: Color.fromARGB(255, 36, 73, 104),
                      ),
                      Icon(
                        Icons.arrow_back,
                        size: 30,
                        color: Color.fromARGB(255, 36, 73, 104),
                      )
                    ],
                  ),
                  Row(
                    children: const [
                      Icon(
                        Icons.euro_rounded,
                        size: 60,
                        color: Color.fromARGB(255, 36, 73, 104),
                      )
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(0, 60, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  DropdownButton(
                    value: dropdownvalue,
                    icon: const Icon(Icons.keyboard_arrow_down),
                    iconSize: 40,
                    elevation: 50,
                    items: items.map((String items) {
                      return DropdownMenuItem(
                        value: items,
                        child: Text(items),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownvalue = newValue!;
                      });
                    },
                  ),
                  SizedBox(
                      height: 60,
                      width: 250,
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.only(left: 30),
                          border: OutlineInputBorder(),
                          labelText: 'Your value',
                        ),
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        keyboardAppearance: Brightness.dark,
                      )),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: const Text('CONVERT TO'),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  DropdownButton(
                    value: dropdownvalue,
                    icon: const Icon(Icons.keyboard_arrow_down),
                    iconSize: 40,
                    elevation: 50,
                    items: items.map((String items) {
                      return DropdownMenuItem(
                        value: items,
                        child: Text(items),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownvalue = newValue!;
                      });
                    },
                  ),
                 const SizedBox(
                      height: 60,
                      width: 250,
                      child: TextField(
                        decoration:  InputDecoration(
                          contentPadding: EdgeInsets.only(left: 30),
                          border: OutlineInputBorder(),
                          labelText: 'Your value',
                        ),
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.done,
                        // inputFormatters: <TextInputFormatter>[
                        //   FilteringTextInputFormatter.digitsOnly
                        // ],
                        keyboardAppearance: Brightness.dark,
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
