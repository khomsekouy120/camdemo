import 'package:camdemo/presentations/auth/resetpassword.dart';
import 'package:flutter/material.dart';

import '../../route/route_name.dart';
// import '../../auth/resetpassword.dart';
// import '../../route/route_name.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _navigate();
  }

  _navigate() async {
    await Future.delayed(const Duration(seconds: 1), () {
      Navigator.pushNamed(context, RouteName.login);
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: Center(
        child: SizedBox(
          child: Hero(
            tag: "logo",
            child: Text("CAMDEMO",
                style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                    fontWeight: FontWeight.bold)),
            // child: Image.network(
            //   "https://play-lh.googleusercontent.com/WLCI3IWMhRh4iZxaun6r0n1a7svwL4A0yjO3p8QHZR_d2zfarLMebto7Goqk_3DeyJ8",
            //   fit: BoxFit.cover,
            //   ),
          ),
        ),
      ),
    );
  }
}
