import 'package:flutter/material.dart';


// ignore: camel_case_types
class Transaction_history extends StatefulWidget {
  const Transaction_history({Key? key}) : super(key: key);

  @override
  State<Transaction_history> createState() => _Transaction_historyState();
}

// ignore: camel_case_types
class _Transaction_historyState extends State<Transaction_history> {
  List<User> users = [
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
    User(
        title: "Camsolution Company",
        description: "For buy computer for internship staff",
        money: '\$400'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 27, 49, 67),
        title: Row(
          children: [
            IconButton(
              icon: const Icon(Icons.keyboard_arrow_left),
              iconSize: 30,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            const Spacer(),
            IconButton(
              icon: const Icon(Icons.menu),
              iconSize: 30,
              onPressed: () {},
            ),
            const Spacer(),
            const Text(
              'TRANSACTIONS',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const Spacer(),
            IconButton(
              icon: const Icon(Icons.settings),
              iconSize: 30,
              onPressed: () {},
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
          child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 150,
            color: const Color.fromARGB(255, 27, 49, 67),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(
                        const EdgeInsets.fromLTRB(25, 25, 25, 25)),
                    backgroundColor: MaterialStateProperty.all(
                        const Color.fromARGB(255, 70, 165, 243)),
                  ),
                  child: const Text(
                    'COMPLETE',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: () {},
                ),
                ElevatedButton(
                  onPressed: () {
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => Tranction_historyItem()));
                  },
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all(
                          const EdgeInsets.fromLTRB(25, 25, 25, 25)),
                      backgroundColor: MaterialStateProperty.all(
                          const Color.fromARGB(255, 70, 165, 243))),
                  child: const Text(
                    'IN PROGRESS',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: users.map((userone) {
              return SizedBox(
                child: Container(
                  width: double.infinity,
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: const Color(0xff7c94b6),
                            image: const DecorationImage(
                              image: AssetImage(
                                'images/mey.jpg',
                              ),
                              fit: BoxFit.cover,
                            ),
                            border: Border.all(
                              width: 35,
                            ),
                            borderRadius: BorderRadius.circular(80),
                          ),
                        ),
                        const Spacer(),
                        Container(
                          width: 200,
                          child: Column(
                            children: [
                              Text(userone.title,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 23)),
                              Text(userone.description,
                                  style: const TextStyle(
                                      color: Color.fromARGB(255, 54, 53, 53))),
                            ],
                          ),
                        ),
                        const Spacer(),
                        Row(
                          children: [
                            Text(
                              userone.money,
                              style: const TextStyle(
                                  color: Color.fromARGB(255, 70, 67, 67),
                                  fontSize: 20),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
        ],
      )),
    );
  }
}

class User {
  String title, description, money;
  User({required this.title, required this.description, required this.money});
}
