import 'package:camdemo/presentations/route/route_name.dart';
import 'package:flutter/material.dart';

import '../../../core/constant.dart';

class Transaction extends StatefulWidget {
  const Transaction({Key? key}) : super(key: key);

  @override
  State<Transaction> createState() => _TransactionState();
}

class _TransactionState extends State<Transaction> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 35,
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 27, 49, 67),
       title: Row(
          children: [
            IconButton(
             icon: const Icon(Icons.keyboard_arrow_left),
              iconSize: 30,
              onPressed: (){ 
                Navigator.pop(context);
              },
            ),
            // const Spacer(),
              IconButton(
             icon: const Icon(Icons.menu),
              iconSize: 30,
              onPressed: (){ 

              },
            ),
            const Spacer(),
            const Text(
              'TRANSACTION',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            const Spacer(),
             IconButton(
             icon: const Icon(Icons.settings),
              iconSize: 30,
              onPressed: (){ 

              },
            ),
          ],
        ),      ),
      body: SingleChildScrollView(
        child: Column(children: [

          Container(
            width: double.infinity,
            height: 450,
            color: const Color.fromARGB(255, 27, 49, 67),
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 50),
                    // padding: const EdgeInsets.all(10),
                    child: const Text("SCAN THIS QR CODE",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 35),
                    child: const Image(
                      image: AssetImage(
                        'images/qr.png',
                      ),
                      width: 200,
                      height: 200,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding:const  EdgeInsets.all(10),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 10),
                    child: const Text("Try BARCODE",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  Container(
                    child: const Image(
                      image: AssetImage(
                        'images/barcode.png',
                      ),
                      width: 300,
                      height: 200,
                      fit: BoxFit.fill,
                     
                    ),
                    
                  ),
                  Container(  
                      child: const Text("can't scan the QR Code?",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  TextButton(
                    onPressed: () {
                    },
                    child:
                    const Text(
                    'Bank Account',
                      style: TextStyle(
                        color: Color.fromARGB(255, 5, 190, 236),
                      ),
                    ),
                  ),
          
           
                 
        
                ],
              ),
            ),
          ),
          
        ]),
      ),
    );
  }
}
