import 'package:camdemo/core/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/framework.dart';

class EditeAccount extends StatefulWidget {
  const EditeAccount({Key? key}) : super(key: key);

  @override
  State<EditeAccount> createState() => _EditeAccountState();
}

class _EditeAccountState extends State<EditeAccount> {
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 44,
        elevation: 0.0,
        backgroundColor: const Color.fromARGB(255, 27, 49, 67),
        title: Row(
          children: [
            const Text('ACCOUNT'),
            const Spacer(),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.settings,
                size: 20,
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Form(
            key: formKey,
            child: Column(
              children: [
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: (const Icon(
                      Icons.person,
                      size: 100,
                      color: Color.fromARGB(255, 104, 170, 224),
                    ))),
                Container(
                  alignment: Alignment.center,
                  // padding: const EdgeInsets.all(10),
                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  
                  child: TextFormField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Your Names",
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'input your usernames';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    // padding: const EdgeInsets.all(10),
                    padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                    child: TextFormField(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Enter your bank number"),
                      validator: (value) {
                        if (value == null || value.isEmpty ) {
                          return 'input your bank number';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                )),
                Container(
                  alignment: Alignment.center,
                  // padding: const EdgeInsets.all(10),
                  
                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "enter a valid email",
                    ),
                    validator: (val) =>
                        val!.isEmpty || !val.contains("@gmail.com")
                            ? "Email"
                            : null,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  // padding: const EdgeInsets.all(5),
                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Password",
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty || value == 6) {
                        return 'input right password';
                      }
                      return null;
                    },
                    obscureText: true,
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    // padding: const EdgeInsets.all(10),
                    padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                    child: TextFormField(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Phone number"),
                      validator: (value) {
                        if (value == null || value.isEmpty || value == 6) {
                          return 'input your phone number';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                    )),
                Container(
                  alignment: Alignment.center,
                  // padding: const EdgeInsets.all(10),
                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                  child: TextFormField(
                    
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "your address",
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'input your address';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    // padding: const EdgeInsets.all(10),
                    padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                    child: const Text(
                        '*Note: you can change your account and we will send notification in your phone device. ')),
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    children:[
                      ElevatedButton(
                        child: Text('SAVE CHANGES'),
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                          }
                        },
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.fromLTRB(20, 20, 25, 20)),
                          backgroundColor: MaterialStateProperty.all(
                              kContentColorLightTheme),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ]),
      )
    );
  }
}
