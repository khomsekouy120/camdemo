class RouteName {
  static const String splash = "/";
  static const String login = "login";
  static const String account = "account";
  static const String resetpassword = "resetpassword";
  static const String otp = "otp";
  static const String create_card = "create_card";
  static const String payment = "payment";
  static const String transfer = "transfer";
  static const String register = 'register';
  static const String transaction = 'transaction';
  static const String transaction_history = 'transaction_history';
  static const String exchange = 'exchange';
  static const String edite_account = "edite_account";
}
