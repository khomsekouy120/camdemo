

import 'package:camdemo/presentations/auth/login.dart';
import 'package:camdemo/presentations/auth/resetpassword.dart';
import 'package:camdemo/presentations/auth/verifycation.dart';
import 'package:camdemo/presentations/route/route_name.dart';
import 'package:camdemo/presentations/screen/create_card/create_card.dart';
import 'package:camdemo/presentations/screen/edite_account/edite_account.dart';
import 'package:camdemo/presentations/screen/splash_screen/splash_screen.dart';
import 'package:camdemo/presentations/screen/transfer/transfer.dart';
import 'package:flutter/material.dart';
import '../auth/register.dart';
import '../screen/account/account.dart';
import '../screen/payment/payment.dart';

import 'package:camdemo/presentations/screen/exchange/exchange.dart';

import '../screen/transaction/transaction.dart';
import '../screen/transaction/transaction_history.dart';
class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case RouteName.splash:
        return MaterialPageRoute(builder: ((context) => const SplashScreen()));
      case RouteName.login:
        return MaterialPageRoute(builder: ((context) => const Login()));
      case RouteName.resetpassword:
        return MaterialPageRoute(builder: (context) => const ResetPassword());
      case RouteName.otp: 
        return MaterialPageRoute(builder: (context) => const VerificationCode());  
      case RouteName.create_card:
        return MaterialPageRoute(builder: ((context) => const CreateCard()));
      case RouteName.payment:
        return MaterialPageRoute(builder: ((context) => const Payment()));
      case RouteName.transfer:
        return MaterialPageRoute(builder: ((context) => const Transfer()));
      case RouteName.register: 
        return MaterialPageRoute(builder: ((context) => const Register()));
      case RouteName.account:
        return MaterialPageRoute(builder: ((context) => const Account()));
      case RouteName.transaction:
        return MaterialPageRoute(builder: ((context) => const Transaction()));
      case RouteName.edite_account:
        return MaterialPageRoute(builder: ((context) => const EditeAccount()));
      case RouteName.transaction_history:
        return MaterialPageRoute(builder: ((context) => const Transaction_history()));
        case RouteName.exchange: 
        return MaterialPageRoute(
          builder: (_) => const Exchange());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Error"),
        ),
        body: const Center(
          child: Text("Error"),
        ),
      );
    });
  }
}
