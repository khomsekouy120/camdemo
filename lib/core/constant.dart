import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF27AE60);
const kPrimaryColorLight = Color(0xFFE9F7EF);
const kSecondaryColor = Color(0xFFE9F7EF);
const kContentColorLightTheme = Color(0xFF1D1D35);
const kContentColorDarkTheme = Color(0xFFF5FCF9);
const kContainerBackgroundColor = Color(0xFFF4F5F7);
const kWarninngColor = Color(0xFFF3BB1C);
const kErrorColor = Color(0xFFF03738);
const kHintColor = Color(0xFFC2C4D2);
const kDividerColor = Color(0xFFF4F4F8);
const kDefaultPadding = 16.0;

const kRadiusControl = 10.0;
const kRadiusSmallControl = 8.0;
const kRadiusContainer = 32.0;
const kRadiusInnerContainer = 24.0;
const kRadiusThirdContainer = 16.0;

const kDefaultAnimDuration = 250;